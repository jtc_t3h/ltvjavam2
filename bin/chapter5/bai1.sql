--1. Liệt kê danh sách nhân viên gồm có các thông tin sau: họ tên, ngày sinh, địa chỉ.
SELECT ho_ten, Ngay_sinh, Dia_chi FROM nhan_vien;

--4. Liệt kê danh sách các nhân viên (họ tên) của đơn vị có ID=1 có thể được phân công vào loại công việc có
--ID=2.

select DISTINCT nv.Ho_ten, lcv.Ten cong_viec
from nhan_vien nv, kha_nang kn, yeu_cau yc, loai_cong_viec lcv
where nv.id = kn.ID_NHAN_VIEN
and kn.ID_NGOAI_NGU = yc.ID_NGOAI_NGU
and yc.ID_LOAI_CONG_VIEC = lcv.ID
and nv.ID_DON_VI = 2
and yc.ID_LOAI_CONG_VIEC = 2;