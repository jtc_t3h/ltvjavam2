package chapter8;

public class Producer extends Thread {

	int count = 0;
	CouponMachine cpm;

	Producer(CouponMachine cpm) {
		this.cpm = cpm;
	}

	public void run() {
		for (int i = 0; i < 5; i++) {
			cpm.createCoupon(++count);
			System.out.println("Coupon produced: " + count);
		}
	}

}
