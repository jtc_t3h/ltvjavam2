package chapter8;

/** 
 * Buoc 1: tao lop thua ke Thread
 * @author hv
 *
 */
public class MyThread extends Thread{

	private String name;
	
	public MyThread(String name) {
		this.name = name;
	}
	
	@Override
	public void run() {
		for (int i = 1; i <= 1000; i++) {
			System.out.println(name + ": " + i);
		}
	}
	
	
	
	public static void main(String[] args) {
		MyThread thread1 = new MyThread("Thread 1");
	}
}
