package chapter8;

public class MyRunable implements Runnable{
	private String name;
	
	public MyRunable(String name) {
		this.name = name;
	}
	
	@Override
	public void run() {
		for (int i = 1001; i <= 2000; i++) {
			System.out.println(name + ": " + i);
		}
	}

}
