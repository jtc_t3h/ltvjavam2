package chapter8;

public class TestCouponMachine {

	public static void main(String[] args) {
		// create the coupon machine.
		CouponMachine cm = new CouponMachine();
		
		Consumer[] con = new Consumer[5];
		for (int i = 0; i < 5; i++) {
			con[i] = new Consumer(cm);
			con[i].setPriority(Thread.MIN_PRIORITY);
			con[i].start();
		}
		
		Producer prod = new Producer(cm);
		prod.setPriority(Thread.MAX_PRIORITY);
		prod.start();
		
		System.out.println("End main thread!");
	}

}
