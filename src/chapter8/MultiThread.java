package chapter8;

import java.util.Scanner;

public class MultiThread {

	public static void main(String[] args) {
		
		// Buoc 2: tao doi tuong 
		MyThread thread1 = new MyThread("Thread 1");
		
		// Buoc 3: thuc thi luong
		thread1.start();
		
		MyRunable myRunable = new MyRunable("Thread 2");
		Thread thread2 = new Thread(myRunable);
		thread2.start();
		
		// Annonymous inner class
		Runnable runnable3 = new Runnable() {
			
			@Override
			public void run() {
				Scanner sc = new Scanner(System.in);
				System.out.print("Nhan n: ");
				int n = sc.nextInt();
				
				for (int i = 2001; i <= n; i++) {
					System.out.println("Thread 3: " + i);
				}
			}
		};
		Thread thread3 = new Thread(runnable3);
		thread3.start();
		
		// Lambda expression
//		Runnable runnable4 = () ->{
//			for (int i = 3001; i <= 4000; i++) {
//				try {
//					Thread.sleep(10);
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				System.out.println("Thread 4: " + i);
//			}
//		};
//		Thread thread4 = new Thread(runnable4);
//		thread4.start();
		
		thread1.getName();  // Dang chay trong Main thread
		System.out.println("Done.");
	}

}
