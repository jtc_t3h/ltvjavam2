package chapter8;

public class CouponMachine {
	private int couponID;
	private boolean couponExists = false;

	public synchronized void createCoupon(int coup) {
		while (couponExists) {
			try {
				wait();
			} catch (Exception e) {
				System.out.println("Exception: " + e);
			}
		}
		this.couponID = coup;
		couponExists = true;
		notify();
	}

	public synchronized int consumeCoupon() {
		while (!couponExists) {
			try {
				wait();
			} catch (Exception e) {
				System.out.println("Exception: " + e);
			}
		}
		couponExists = false;
		notify();
		return couponID;
	}

}
