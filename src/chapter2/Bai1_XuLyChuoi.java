package chapter2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Bai1_XuLyChuoi {

	public static void main(String[] args) {
		
		/**
		 * Sinh ra 15 phan tu va tao ra list 1 chua 15 phan tu do
		 */
		List<String> list1 = new ArrayList<>();
		
		// Nhap 15 phan tu
		Scanner sc = new Scanner(System.in);
		System.out.println("Nhap 5 phan tu cua list 1:");
		for (int idx = 0; idx < 5; idx++) {
			list1.add(sc.nextLine());
		}
		
		// task: in cac phan tu cua list 1
		Stream<String> stream1 = list1.stream();
		stream1.forEach(element -> System.out.println(element));
		
		list1.stream().forEach(element -> System.out.println(element));
		
		// task: trong list 1 đếm có bao nhiêu phần tử là "" (rỗng)
		long emptyCount = list1.stream().filter(element -> element.isEmpty()).count();
		System.out.printf("Trong list 1: có %s phần tử rỗng.", emptyCount);

		/*
		 * Tạo ra list 2 từ list 1 bao gồm các phần tử khác rỗng
		 */
		List<String> list2 = list1.stream().filter(element -> !element.isEmpty()).
				collect(Collectors.toList());
		System.out.println("Các phần tử của List 2:");
		list2.stream().forEach(element -> System.out.println(element));
		
	}

}
