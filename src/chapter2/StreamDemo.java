package chapter2;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.print.attribute.IntegerSyntax;

public class StreamDemo {

	public static void main(String[] args) {
		// ND1. Tao Stream
		// C1. dung Stream.of
		Stream<Integer> stream1 = Stream.of(1,2,3,4,5,6,7,8,9,10);
		Stream<Integer> stream2 = Stream.of(new Integer[] {11,12,13,14});
				
		stream2.forEach(e -> System.out.println(e));
		stream1.map(e -> e * e).filter(e -> e <= 50).forEach(e -> System.out.println(e));
		
		// C2. dung stream hoac parallelStream cua doi tuong co kieu Collection
		List<String> list = Arrays.asList("hello", "world", "java", "programming");
		Stream<String> stream3 = list.stream();
		
		list = stream3.sorted().peek(e -> System.out.println(e)).collect(Collectors.toList());
		
		// C3. dung Arrays.stream
		Stream<String> stream4 = Arrays.stream(new String[] {"khoa", "hoc", "tu", "nhien"});
		String khtn = stream4.collect(Collectors.joining(" "));
		System.out.println(khtn);
		
		// Tổng giá trị của tất cả các phần tử trong Stream1
		Stream<Integer> stream5 = Stream.of(1,2,3,4);
		int sum = stream5.reduce((t, u) -> t + u).get();
		System.out.println("sum = " + sum);
		
		// Trung bình của tất cả các phần tử trong Stream 2
		Stream<Integer> stream6 = Stream.of(1,2,3,4);
		int avg = (int) stream6.mapToInt(t -> t).summaryStatistics().getAverage();
		System.out.println("avg = " + avg);
		
		// Tao stream dung ham generate cua lop Stream
		Stream<String> stream7 = Stream.generate(() -> "abc");
	}

}
