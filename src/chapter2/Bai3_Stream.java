package chapter2;

import java.util.ArrayList;
import java.util.List;

public class Bai3_Stream {

	public static void main(String[] args) {

		// Tạo ra một List kiểu Student có 10 phần tử. In List 1 vừa tạo.
		List<Bai3_Student> list = new ArrayList<Bai3_Student>();
		
		Bai3_Student stu1 = new Bai3_Student("Pham Nghia", 25, 10F, 10F);
		Bai3_Student stu2 = new Bai3_Student("Thuc Nguyen", 33, 7, 8);
		Bai3_Student stu3 = new Bai3_Student("Khuong Le", 30, 8, 7);
		Bai3_Student stu4 = new Bai3_Student("Ngoc Phu", 30, 6, 8);
		Bai3_Student stu5 = new Bai3_Student("Thuoc Thang", 22, 7, 7);
		
		list.add(stu1);
		list.add(stu2);
		list.add(stu3);
		list.add(stu4);
		list.add(stu5);
		
		System.out.println("List of student:");
		list.forEach(student -> System.out.println(student));
		
		// Cho biết trong danh sách Student này có bao nhiêu bạn có tuổi >=30
		System.out.println("Number of student have old >= 30: " +
				list.stream().filter(student -> student.getAge() >= 30).count());
		
		// Cho biết trong danh sách này có bao nhiêu Student nào có firstName bắt đầu là “T”. In ra sinh viên
		// đầu tiên trong danh sách có firstName bắt đầu là “T”
		System.out.println("Number of student have name start with T: " + 
				list.stream().filter(student -> student.getName().startsWith("T")).count());
		System.out.println("First student \n" + 
				list.stream().filter(student -> student.getName().startsWith("T")).findFirst().get());
	}

}
