package chapter6;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexDemo {

	public static void main(String[] args) {

		String regex = "^[a-z0-9_-]{6,20}$" ; // mẫu biểu diễn theo REGEX
		Pattern p = Pattern.compile(regex);
		
		String input = "administrta";  // chuỗi cần kiểm tra
		Matcher matcher = p.matcher(input);
		
		System.out.println(matcher.matches());
		matcher.reset();
		System.out.println(matcher.find());
		
		
	}

}
