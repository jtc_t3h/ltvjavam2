package chapter6;

import java.util.Locale;
import java.util.ResourceBundle;

public class MultiLanguageDemo {

	public static void main(String[] args) {
		
		String language = "en";
		String country = "US";
		Locale locale = new Locale(language, country);
		ResourceBundle bundle = ResourceBundle.getBundle("chapter6.ApplicationResource", locale);
		
		System.out.println(bundle.getString("username"));
		System.out.println(bundle.getString("password"));
	}

}
