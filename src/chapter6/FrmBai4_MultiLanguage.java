package chapter6;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JList;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Enumeration;
import java.util.Locale;
import java.util.ResourceBundle;

public class FrmBai4_MultiLanguage extends JFrame {

	private JPanel contentPane;
	private ButtonGroup groupLanguage = new ButtonGroup();
	private JList list;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai4_MultiLanguage frame = new FrmBai4_MultiLanguage();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai4_MultiLanguage() {
		setTitle("Resource Bundle cho ngôn ngữ");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 544);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblChnNgnNg = new JLabel("Chọn ngôn ngữ");
		lblChnNgnNg.setBounds(10, 11, 105, 14);
		contentPane.add(lblChnNgnNg);
		
		JRadioButton rdbtnVietnamese = new JRadioButton("Vietnamese");
		rdbtnVietnamese.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String languageCode = "vi";
				String countryCode = "VN";
				loadResouceBundle(languageCode, countryCode);
			}
		});
		rdbtnVietnamese.setSelected(true);
		rdbtnVietnamese.setBounds(149, 7, 109, 23);
		contentPane.add(rdbtnVietnamese);
		groupLanguage.add(rdbtnVietnamese);
		
		JRadioButton rdbtnEnglish = new JRadioButton("English");
		rdbtnEnglish.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String languageCode = "en";
				String countryCode = "US";
				loadResouceBundle(languageCode, countryCode);
			}
		});
		rdbtnEnglish.setBounds(283, 7, 109, 23);
		contentPane.add(rdbtnEnglish);
		groupLanguage.add(rdbtnEnglish);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 45, 414, 450);
		contentPane.add(scrollPane);
		
		list = new JList();
		scrollPane.setViewportView(list);
		
		// Hien thi mac dinh
		String languageCode = "vi";
		String countryCode = "VN";
		loadResouceBundle(languageCode, countryCode);
	}

	protected void loadResouceBundle(String languageCode, String countryCode) {
		Locale currentLocal = new Locale(languageCode, countryCode);
		
		ResourceBundle message = ResourceBundle.getBundle("chapter6.Bai4_Resource", currentLocal);
		
		DefaultListModel<String> model = new DefaultListModel<>();
		
		Enumeration<String> keys = message.getKeys();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			String value = message.getString(key);
			
			model.addElement(value);
		}
		list.setModel(model);
	}
}
