package chapter6;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.util.Locale;
import java.util.ResourceBundle;
import java.awt.event.ActionEvent;

public class FrmBai5_VeMayBay extends JFrame {

	private JPanel contentPane;
	private JTextField txtDepartureDate;
	private JTextField txtReturnDate;
	private JButton btnVietnamese;
	private JButton btnEnglish;
	private JLabel lblTicketType;
	private JRadioButton rdbOneWay;
	private JRadioButton rdbRoundTrip;
	private JLabel lblDeparture;
	private JLabel lblDestination;
	private JLabel lblDepartureDate;
	private JLabel lblReturnDate;
	private JLabel lblAdults;
	private JLabel lblChildren;
	private JLabel lblInfant;
	private JComboBox cbbInfant;
	private JButton btnSearch;
	private JComboBox cbbChildren;
	private JComboBox cbbAdults;
	private JComboBox cbbDestination;
	private JComboBox cbbDeparture;
	
	private ButtonGroup btgTicketType = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai5_VeMayBay frame = new FrmBai5_VeMayBay();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai5_VeMayBay() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 413, 350);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		btnVietnamese = new JButton("New button");
		btnVietnamese.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					loadMultiLanguage("vi", "VN");
				} catch (IllegalArgumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IllegalAccessException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnVietnamese.setBounds(10, 11, 73, 23);
		contentPane.add(btnVietnamese);
		
		btnEnglish = new JButton("New button");
		btnEnglish.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					loadMultiLanguage("en", "US");
				} catch (IllegalArgumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IllegalAccessException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnEnglish.setBounds(103, 11, 73, 23);
		contentPane.add(btnEnglish);
		
		lblTicketType = new JLabel("New label");
		lblTicketType.setBounds(10, 59, 130, 14);
		contentPane.add(lblTicketType);
		
		rdbOneWay = new JRadioButton("New radio button");
		rdbOneWay.setBounds(146, 55, 109, 23);
		rdbOneWay.setSelected(true);
		contentPane.add(rdbOneWay);
		btgTicketType.add(rdbOneWay);
		
		rdbRoundTrip = new JRadioButton("New radio button");
		rdbRoundTrip.setBounds(270, 55, 109, 23);
		contentPane.add(rdbRoundTrip);
		btgTicketType.add(rdbRoundTrip);
		
		lblDeparture = new JLabel("New label");
		lblDeparture.setBounds(10, 84, 130, 14);
		contentPane.add(lblDeparture);
		
		cbbDeparture = new JComboBox();
		cbbDeparture.setBounds(146, 80, 245, 22);
		contentPane.add(cbbDeparture);
		
		lblDestination = new JLabel("New label");
		lblDestination.setBounds(10, 113, 130, 14);
		contentPane.add(lblDestination);
		
		cbbDestination = new JComboBox();
		cbbDestination.setBounds(146, 109, 245, 22);
		contentPane.add(cbbDestination);
		
		lblDepartureDate = new JLabel("New label");
		lblDepartureDate.setBounds(10, 144, 130, 14);
		contentPane.add(lblDepartureDate);
		
		txtDepartureDate = new JTextField();
		txtDepartureDate.setBounds(146, 141, 130, 20);
		contentPane.add(txtDepartureDate);
		txtDepartureDate.setColumns(10);
		
		lblReturnDate = new JLabel("New label");
		lblReturnDate.setBounds(10, 173, 130, 14);
		contentPane.add(lblReturnDate);
		
		txtReturnDate = new JTextField();
		txtReturnDate.setColumns(10);
		txtReturnDate.setBounds(146, 170, 130, 20);
		contentPane.add(txtReturnDate);
		
		lblAdults = new JLabel("New label");
		lblAdults.setBounds(10, 202, 130, 14);
		contentPane.add(lblAdults);
		
		cbbAdults = new JComboBox();
		cbbAdults.setBounds(146, 198, 130, 22);
		contentPane.add(cbbAdults);
		
		lblChildren = new JLabel("New label");
		lblChildren.setBounds(10, 231, 130, 14);
		contentPane.add(lblChildren);
		
		cbbChildren = new JComboBox();
		cbbChildren.setBounds(146, 227, 130, 22);
		contentPane.add(cbbChildren);
		
		lblInfant = new JLabel("New label");
		lblInfant.setBounds(10, 260, 130, 14);
		contentPane.add(lblInfant);
		
		cbbInfant = new JComboBox();
		cbbInfant.setBounds(146, 256, 130, 22);
		contentPane.add(cbbInfant);
		
		btnSearch = new JButton("New button");
		btnSearch.setBounds(146, 289, 142, 23);
		contentPane.add(btnSearch);
		
		//
		try {
			loadMultiLanguage();
		} catch (IllegalArgumentException | IllegalAccessException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	protected void loadMultiLanguage(String ... codes) throws IllegalArgumentException, IllegalAccessException {
		
		String languageCode = "vi";
		String countryCode = "VN";
		
		if (codes.length == 2) {
			languageCode = codes[0];
			countryCode = codes[1];
		} 
		
		Locale locale = new Locale(languageCode, countryCode);
		ResourceBundle message = ResourceBundle.getBundle("chapter6.Bai5_Resource", locale);
		
		Field[] fields = FrmBai5_VeMayBay.class.getDeclaredFields();
		for (Field field: fields) {
			System.out.println(field.getName() + "===" + field.getType());
			
			Object temp = field.get(this);
			if (temp instanceof JLabel) {
				JLabel lbl = (JLabel) temp;
				lbl.setText(message.getString(field.getName()));
			} else if (temp instanceof JButton) {
				JButton btn = (JButton) temp;
				btn.setText(message.getString(field.getName()));
			}
			
		}
		
	}
}
