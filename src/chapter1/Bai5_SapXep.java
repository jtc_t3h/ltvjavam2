package chapter1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Bai5_SapXep {

	public static void main(String[] args) throws IOException {
		
		// B1. Tao danh sach cac chuoi
		// P1: Tao mang tinh
		String arr[] = {"hello", "world", "java", "programing", "language"};
		
		// P2: Tao List tu nguoi dung nhap
		List<String> list = new ArrayList<String>();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Nguoi dung nhap du lieu (-1 la ket thuc):");
		while (true) {
			String element = br.readLine();
			if ("-1".equals(element)) {
				break;
			}
			list.add(element);
		}
		
		
		// B2. Sap xep 
		// YC1. tu chuoi co chieu dai nho -> lon
		List<String> listFromArr = Arrays.asList(arr);
		listFromArr.sort((o1, o2) -> o1.length() - o2.length());
		
		System.out.println();
		System.out.println("YC1. tu chuoi co chieu dai nho -> lon");
		listFromArr.forEach(e -> System.out.println(e));
		
		// YC2. tu chuoi co chieu dai lon -> nho
		list.sort((o1, o2) -> o2.length() - o1.length());
		
		System.out.println();
		System.out.println("YC2. tu chuoi co chieu dai lon -> nho");
		list.forEach(e -> System.out.println(e));
		
		// YC3. sap xep theo alphabet
		list.sort((o1, o2) -> o1.compareTo(o2));
		
		System.out.println();
		System.out.println("YC3. sap xep theo alphabet");
		list.forEach(e -> System.out.println(e));
	}

}
