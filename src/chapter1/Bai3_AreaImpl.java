package chapter1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Bai3_AreaImpl {

	public static void main(String[] args) throws NumberFormatException, IOException {
		
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Nhập bán kinh: ");
		double r = Double.parseDouble(input.readLine());
		
		Bai3_Area circle = (double x1, double x2) -> x1 * Math.pow(x2, 2);
		System.out.println("S = PI * (r*r) = " + circle.calArea(Math.PI, r));
		
		System.out.println("Nhập chiều dài: ");
		double H = Double.parseDouble(input.readLine());
		System.out.println("Nhập chiều rộng: ");
		double W = Double.parseDouble(input.readLine());
		
		Bai3_Area rectangle = (double x1, double x2) -> x1 * x2;
		System.out.println("S = W x H = " + rectangle.calArea(W, H));
	}

}
