package chapter1;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class TestClient {

	public static void main(String[] args) {

		// PP1: Su dung lop Implement tuong minh
		Calculator c1 = new CalculatorImpl();
		System.out.println(c1.operator(1.1, 2.2));
		
		// PP2: Su dung lop Implemt voi anonymous inner class
		Calculator c2 = new Calculator() {
			
			@Override
			public double operator(double a, double b) {
				return a - b;
			}
		};
		System.out.println(c2.operator(2.2, 1.1));
		
		// PP3: Dung lambda expression
		Calculator c3 = (double a, double b) -> a * b;
		System.out.println(c3.operator(1.1, 2.2));
		
		Calculator c4 = (a, b) -> a / b;
		System.out.println(c4.operator(4.0, 2.0));
		
		// Tao list tu danh sach cac phan tu 
		// Duyet cac phan tu cua list
		List<Integer> list = Arrays.asList(1,2,3,4,5,6,7,8,9);
		list.forEach((Integer e) -> System.out.println(e));
			
		// Sort theo thu tu tu Lon -> Be
		// PP1: dung anonymous inner class
		Comparator<Integer> comparator = new Comparator<Integer>() {

			@Override
			public int compare(Integer o1, Integer o2) {
				return o2 - o1;
			}
			
		};
		list.sort(comparator);
		System.out.println("Sort tu lon -> nho:");
		list.forEach((Integer e) -> System.out.println(e));
		
		// Sort tu nho -> lon dung PP lambda expression
		list.sort((Integer o1, Integer o2) -> o1 - o2);
		System.out.println("Sort tu nho -> lon:");
		list.forEach((Integer e) -> System.out.println(e));
	}

}
