package chapter1;

public interface Calculator {

	public double operator(double a, double b);
}
