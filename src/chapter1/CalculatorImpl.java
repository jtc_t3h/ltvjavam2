package chapter1;

public class CalculatorImpl implements Calculator{

	@Override
	public double operator(double a, double b) {
		return a + b;
	}

}
