package chapter4;

public class Bai2_DonVi {

	private long id;
	private String name;
	
	public Bai2_DonVi(){}
	
	public Bai2_DonVi(long id, String name) {
		this.id = id;
		this.name = name;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return name;
	}
}
