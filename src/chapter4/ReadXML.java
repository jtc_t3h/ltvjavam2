package chapter4;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ReadXML {

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.parse(new File("src/chapter4/contact.xml"));
		
		// root node
		Element root = doc.getDocumentElement();
		System.out.println("root = " + root.getNodeName());
		
		// children of root node
		NodeList rootChildren = root.getChildNodes();
		System.out.println("rootChildren length = " + rootChildren.getLength());
		for (int idx = 0; idx < rootChildren.getLength(); idx++) {
			Node child = rootChildren.item(idx);
			if (child.getNodeType() == Node.ELEMENT_NODE) {
				System.out.println(child.getNodeName());
			}
		}
		
	}

}
