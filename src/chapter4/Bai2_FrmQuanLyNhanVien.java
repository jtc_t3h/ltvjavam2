package chapter4;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class Bai2_FrmQuanLyNhanVien extends JFrame {

	private JPanel contentPane;
	private JTable tblNhanVien;
	private JComboBox cbbDonVi;
	
	private List<Bai2_NhanVien> listOfNhanVien = null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Bai2_FrmQuanLyNhanVien frame = new Bai2_FrmQuanLyNhanVien();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Bai2_FrmQuanLyNhanVien() {
		setTitle("Quản lý nhân viên");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 745, 401);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblnV = new JLabel("Đơn vị");
		lblnV.setBounds(10, 11, 65, 14);
		contentPane.add(lblnV);

		cbbDonVi = new JComboBox();
		cbbDonVi.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				Bai2_DonVi donVi = (Bai2_DonVi) cbbDonVi.getSelectedItem();
				if (donVi.getId() != 0) {
					System.out.println("=========================");
					updateTable();
				}
			}
		});
		cbbDonVi.setBounds(101, 8, 517, 20);
		contentPane.add(cbbDonVi);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 63, 709, 289);
		contentPane.add(scrollPane);

		tblNhanVien = new JTable();
		tblNhanVien.setModel(new DefaultTableModel(new Object[][] {}, new String[] { "ID", "H\u1ECD t\u00EAn",
				"Gi\u1EDBi t\u00EDnh", "Ng\u00E0y sinh", "M\u1EE9c l\u01B0\u01A1ng", "\u0110\u1ECBa ch\u1EC9" }));
		tblNhanVien.getColumnModel().getColumn(0).setPreferredWidth(36);
		scrollPane.setViewportView(tblNhanVien);

		// Nap danh sach don vi cho cbbDonvi
		initDataForCbbDonVi();
		
		// Danh sách nhân viên
		listOfNhanVien = readXMLByNhanVien();
	}

	protected void updateTable() {

		// Đơn vị được chọn
		Bai2_DonVi donVi = (Bai2_DonVi) cbbDonVi.getSelectedItem();
		
		// Lọc danh sách nhân viên theo đơn vị đã chọn
		List<Bai2_NhanVien> listNhanVienAfterFilter = listOfNhanVien.stream().filter(nhanVien -> nhanVien.getIdDonVi() == donVi.getId()).collect(Collectors.toList());
		System.out.println("2=============" + listNhanVienAfterFilter.size());
		
		// Hiển thị danh sách nhân viên đã lọc lên frame
		DefaultTableModel model = (DefaultTableModel) tblNhanVien.getModel();
	    
		// Xóa dữ liệu trước đó
		while (model.getRowCount() > 0) {
			model.removeRow(model.getRowCount() - 1);
		}
		
		listNhanVienAfterFilter.forEach(nv -> {
			model.addRow(new Object[] {nv.getId(), nv.getName(), nv.getGender(), nv.getBirthday(), nv.getSalary(), nv.getAddress()});
		});
		tblNhanVien.setModel(model);
		
	}

	private List<Bai2_NhanVien> readXMLByNhanVien() {
		List<Bai2_NhanVien> listOfNhanVien = new ArrayList<Bai2_NhanVien>();

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		Document doc;
		try {
			builder = factory.newDocumentBuilder();
			doc = builder.parse(new FileInputStream("./src/chapter4/bai2_nhan_vien.xml"));

			NodeList nhanViens = doc.getElementsByTagName("nhan_vien");
			for (int idx = 0; idx < nhanViens.getLength(); idx++) {
				Bai2_NhanVien nhanVien = new Bai2_NhanVien();
				Element nhanVienXML = (Element) nhanViens.item(idx);

				nhanVien.setId(Long.parseLong(nhanVienXML.getElementsByTagName("id").item(0).getTextContent()));
				nhanVien.setName(nhanVienXML.getElementsByTagName("ho_ten").item(0).getTextContent());
				nhanVien.setGender("1".equals(nhanVienXML.getElementsByTagName("gioi_tinh").item(0).getTextContent())?"Nam":"Nữ");
				nhanVien.setBirthday(nhanVienXML.getElementsByTagName("ngay_sinh").item(0).getTextContent());
				nhanVien.setSalary(Long.parseLong(nhanVienXML.getElementsByTagName("muc_luong").item(0).getTextContent()));
				nhanVien.setAddress(nhanVienXML.getElementsByTagName("dia_chi").item(0).getTextContent());
				nhanVien.setIdDonVi(Long.parseLong(nhanVienXML.getElementsByTagName("id_don_vi").item(0).getTextContent()));
				
				listOfNhanVien.add(nhanVien);
			}

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return listOfNhanVien;
	}

	private void initDataForCbbDonVi() {

		// Lấy danh sách đơn vị từ file bai2_don_vi.xml
		List<Bai2_DonVi> listOfDonVi = readXMLByDonVi();

		// Nạp danh sách đơn vị vào CbbDonVi
		DefaultComboBoxModel<Bai2_DonVi> model = new DefaultComboBoxModel<Bai2_DonVi>();
		Bai2_DonVi donVi = new Bai2_DonVi(0L, "--- Chọn đơn vị ---");
		model.addElement(donVi);

		for (Bai2_DonVi donvi : listOfDonVi) {
			model.addElement(donvi);
		}
		cbbDonVi.setModel(model);

	}

	private List<Bai2_DonVi> readXMLByDonVi() {
		List<Bai2_DonVi> listOfDonvi = new ArrayList<Bai2_DonVi>();

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		Document doc;
		try {
			builder = factory.newDocumentBuilder();
			doc = builder.parse(new FileInputStream("./src/chapter4/bai2_don_vi.xml"));

			NodeList donVis = doc.getElementsByTagName("don_vi");
			for (int idx = 0; idx < donVis.getLength(); idx++) {
				Element donViXML = (Element) donVis.item(idx);

				String id = donViXML.getElementsByTagName("id").item(0).getTextContent();
				String name = donViXML.getElementsByTagName("ten").item(0).getTextContent();

				Bai2_DonVi donVi = new Bai2_DonVi(Long.parseLong(id), name);
				listOfDonvi.add(donVi);
			}

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return listOfDonvi;
	}
}
