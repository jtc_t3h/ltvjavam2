package chapter3;

import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class WriteFileBySimpleJson2 {

	public static void main(String[] args) throws IOException {

		JSONArray arrBook = new JSONArray();

		JSONObject book1 = new JSONObject();
		book1.put("name", "Java Programming");
		book1.put("edition", 7);
		arrBook.add(book1);

		JSONObject book2 = new JSONObject();
		book2.put("name", "C#");
		book2.put("edition", 3);
		arrBook.add(book2);

		JSONObject book3 = new JSONObject();
		book3.put("name", "Flutter");
		book3.put("edition", 5);
		arrBook.add(book3);

		try (FileWriter file = new FileWriter("./src/chapter3/book_store_out.json")) {

			file.write(arrBook.toJSONString());
			file.flush();

		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Done.");
	}
}
