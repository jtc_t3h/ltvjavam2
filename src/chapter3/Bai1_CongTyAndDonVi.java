package chapter3;

import java.util.List;

public class Bai1_CongTyAndDonVi {

	private Bai1_CongTy[] CONG_TY;
	private List<Bai1_DonVi> DON_VI;

	public Bai1_CongTy[] getCONG_TY() {
		return CONG_TY;
	}

	public void setCONG_TY(Bai1_CongTy[] cONG_TY) {
		CONG_TY = cONG_TY;
	}

	public List<Bai1_DonVi> getDON_VI() {
		return DON_VI;
	}

	public void setDON_VI(List<Bai1_DonVi> dON_VI) {
		DON_VI = dON_VI;
	}

}
