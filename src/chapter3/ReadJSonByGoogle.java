package chapter3;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

public class ReadJSonByGoogle {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws JsonSyntaxException, JsonIOException, FileNotFoundException {
		Gson gson = new Gson();
		
		List<Student> list = gson.fromJson(new FileReader("src/chapter3/student.json"), List.class);
		System.out.println(list.size());
		for (Student s: list) {
			System.out.println(s.getName());
		}
	}

}
