package chapter3;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import org.json.simple.JSONObject;

import com.google.gson.Gson;

public class Bai1_GSon {

	public static void main(String[] args) {
		 Gson gson = new Gson();

	     try (Reader reader = new FileReader("src/chapter3/QLCT_1.json")) {

	            // Convert JSON File to Java Object
	            Bai1_CongTyAndDonVi objA = gson.fromJson(reader, Bai1_CongTyAndDonVi.class);
				
	            System.out.println("***** Thông tin công ty *****");
	    		System.out.println("Tên công ty: " + objA.getCONG_TY()[0].getTen());
	    		
	    		
	    		System.out.println("***** Thông tin đơn vị *****");
	    		for (int idx = 1; idx <= objA.getDON_VI().size(); idx++) {
	    			
	    			Bai1_DonVi donVi = objA.getDON_VI().get(idx - 1);
	    			System.out.println(idx + "/ Tên đơn vị: " + donVi.getTen());
	    			System.out.println("Số nhân viên: " + donVi.getSo_Nhan_vien());
	    		}

	        } catch (IOException e) {
	            e.printStackTrace();
	        }

	}

}
