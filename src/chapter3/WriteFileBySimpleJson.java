package chapter3;

import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class WriteFileBySimpleJson {

	public static void main(String[] args) throws IOException {
		
		
		JSONArray array = new JSONArray();
		
		JSONObject stu1 = new JSONObject();
		stu1.put("name", "Trong Nghia");
		stu1.put("age", 30);
		array.add(stu1);
		
		JSONObject stu2 = new JSONObject();
		stu2.put("name", "Trong Nghia");
		stu2.put("age", 30);
		array.add(stu2);

		try (FileWriter out = new FileWriter("src/chapter3/student_out.json")){
			out.write(array.toJSONString());
			
			out.flush();
		}
		System.out.println("Done.");
	}
}
