package chapter3;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Bai1_JSonSimple {

	public static void main(String[] args) throws FileNotFoundException, IOException, ParseException {
		JSONParser parse = new JSONParser();
		JSONObject objA = (JSONObject) parse.parse(new FileReader("src/chapter3/QLCT_1.json"));

		JSONArray arrCongTy = (JSONArray) objA.get("CONG_TY");
		JSONArray arrDonVi = (JSONArray) objA.get("DON_VI");
		
		System.out.println("***** Thông tin công ty *****");
		
		JSONObject objCongTy = (JSONObject) arrCongTy.get(0);
		System.out.println("Tên công ty: " + objCongTy.get("Ten"));
		
		
		System.out.println("***** Thông tin đơn vị *****");
		for (int idx = 1; idx <= arrDonVi.size(); idx++) {
			
			JSONObject objDonVi = (JSONObject) arrDonVi.get(idx - 1);
			System.out.println(idx + "/ Tên đơn vị: " + objDonVi.get("Ten"));
			System.out.println("Số nhân viên: " + objDonVi.get("So_Nhan_vien"));
		}
	}

}
