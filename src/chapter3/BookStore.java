package chapter3;

import java.util.List;

public class BookStore {

	private List<Book> books;
	private String[] notebook;

	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}

	public String[] getNotebook() {
		return notebook;
	}

	public void setNotebook(String[] notebook) {
		this.notebook = notebook;
	}
	
	
}
