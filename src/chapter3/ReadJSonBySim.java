package chapter3;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ReadJSonBySim {

	public static void main(String[] args) throws FileNotFoundException, IOException, ParseException {

		JSONParser jsonParser = new JSONParser();
		
		JSONArray arrStudent = (JSONArray)jsonParser.parse(new FileReader("src/chapter3/student.json"));
		
		System.out.println(arrStudent.size());
		for (int idx = 0; idx < arrStudent.size(); idx++) {
			JSONObject student = (JSONObject)arrStudent.get(idx);
			
			System.out.println(student.get("name"));
		}
	}

}
