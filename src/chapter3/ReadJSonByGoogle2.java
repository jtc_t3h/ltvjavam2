package chapter3;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

public class ReadJSonByGoogle2 {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws JsonSyntaxException, JsonIOException, FileNotFoundException {
		Gson gson = new Gson();
		
		BookStore bookStore = gson.fromJson(new FileReader("./src/chapter3/book_store.json"), BookStore.class);
		List<Book> listBooks = bookStore.getBooks();
		
		for (Book book: listBooks) {
			System.out.println(book.getName());
		}
		
		System.out.println(bookStore.getNotebook()[0]);
	}

}
