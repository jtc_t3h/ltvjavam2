package chapter3;

public class Student {

	// Thuoc tinh = instance variable
	private String name;
	private int age;
	private float mark1;
	private float mark2;
	private float avg;

	// phuong thuc Khoi tao
	public Student() {
	}

	public Student(String name, int age, float mark1, float mark2) {
		this.name = name;
		this.age = age;
		this.mark1 = mark1;
		this.mark2 = mark2;
	}

	// Phuong thuc getter va setter
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public float getMark1() {
		return mark1;
	}

	public void setMark1(float mark1) {
		this.mark1 = mark1;
	}

	public float getMark2() {
		return mark2;
	}

	public void setMark2(float mark2) {
		this.mark2 = mark2;
	}

	public float getAvg() {
		avg = (mark1 + mark2) / 2;
		return avg;
	}

	public void setAvg(float avg) {
		this.avg = avg;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(name).
		   append("\t").
		   append("age = ").
		   append(age).
		   append("\t").
		   append("mark 1 = ").
		   append(mark1).
		   append(" & mark2 = ").
		   append(mark2).
		   append("\t").
		   append("avg = ").
		   append(getAvg());
		
		return sb.toString();
	}
    
	
}
