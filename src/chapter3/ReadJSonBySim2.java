package chapter3;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ReadJSonBySim2 {

	public static void main(String[] args) throws FileNotFoundException, IOException, ParseException {

		JSONParser parser = new JSONParser();
		
		JSONObject store = (JSONObject) parser.parse(new FileReader("./src/chapter3/book_store.json"));
	
		JSONArray books = (JSONArray) store.get("books");
	
		for (int idx = 0; idx < books.size(); idx++) {
			JSONObject book = (JSONObject) books.get(idx);
			
			System.out.println(book.get("name"));
		}
	}

}
