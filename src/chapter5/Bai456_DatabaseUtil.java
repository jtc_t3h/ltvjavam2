package chapter5;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Bai456_DatabaseUtil {

	private final static String URL = "jdbc:mysql://localhost:3308/qlsach";
	private final static String USERNAME = "root";
	private final static String PASSWORD  = "";
	
	private Connection getConnection() throws SQLException {
		try {
			Connection con = DriverManager.getConnection(URL, USERNAME, PASSWORD);
			return con;
		} catch (SQLException e) {
			throw e;
		}
	}
	
	public int add(Bai456_Sach sach) throws SQLException {
		try (Connection conn = this.getConnection();
				Statement st = conn.createStatement();){
			
			StringBuilder sb = new StringBuilder();
			sb.append("INSERT INTO sach(name, author, isbn, price) VALUES(").
				append("'" + sach.getName() + "', ").
				append("'" + sach.getAuthor() + "', ").
				append("'" + sach.getIsbn() + "', ").
				append(sach.getPrice()).
				append(")");
			
			return st.executeUpdate(sb.toString());
		} catch (SQLException e) {
			throw e;
		}
	}
}
