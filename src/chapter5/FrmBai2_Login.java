package chapter5;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.HeadlessException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;

public class FrmBai2_Login extends JFrame {

	private JPanel contentPane;
	private JTextField txtUsername;
	private JPasswordField txtPassword;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai2_Login frame = new FrmBai2_Login();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai2_Login() {
		setTitle("Login");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 360, 169);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblUsername = new JLabel("Username");
		lblUsername.setBounds(10, 24, 76, 14);
		contentPane.add(lblUsername);
		
		txtUsername = new JTextField();
		txtUsername.setBounds(96, 21, 230, 20);
		contentPane.add(txtUsername);
		txtUsername.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(10, 55, 76, 14);
		contentPane.add(lblPassword);
		
		txtPassword = new JPasswordField();
		txtPassword.setBounds(96, 52, 230, 20);
		contentPane.add(txtPassword);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				// Lay usernam va password nguoi dung nhap
				String username = txtUsername.getText().trim();
				String password = String.valueOf(txtPassword.getPassword());
				
				try {
					if (checkLogin(username, password)) {
						JOptionPane.showMessageDialog(null, "Login is successful.");
					} else {
						JOptionPane.showMessageDialog(null, "Invalid Username or Password.");
					}
				} catch (HeadlessException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});
		btnLogin.setBounds(96, 96, 89, 23);
		contentPane.add(btnLogin);
	}

	/**
	 * Check username and password is exist.
	 * CREATE DATABASE `qllienhe`;
	 * CREATE TABLE `qllienhe`.`user` ( `id` INT NOT NULL AUTO_INCREMENT , `username` VARCHAR(100) NOT NULL , `password` VARCHAR(100) NOT NULL , PRIMARY KEY (`id`)) ENGINE = MyISAM;
	 * @param username
	 * @param password
	 * @return
	 * @throws SQLException 
	 */
	protected boolean checkLogin(String username, String password) throws SQLException {
		boolean ret = false;
		
		String url = "jdbc:mysql://localhost:3308/qllienhe";
		String usernameDB = "root";
		String passwordDB = "";
		
		Connection con = null;
		Statement statement = null;
		ResultSet rs = null;
		try {
			con = DriverManager.getConnection(url, usernameDB, passwordDB);
			statement = con.createStatement();
			
			String sql = "SELECT * FROM user WHERE username = '" + username + "' and password = '" + 
					password + "'";
			rs = statement.executeQuery(sql);
			while (rs.next()) {
				ret = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (rs != null) rs.close();
			if (statement != null) statement.close();
			if (con != null) con.close();
		}
		
		return ret;
	}
}
