package chapter5;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectDBDemo {

	public static void main(String[] args) {

		// Buoc 1: nap trinh dieu kien -> import thu vien
		// Cach kiem tra thu vien da import hay chua
		final String DRIVER_CLASS = "com.mysql.cj.jdbc.Driver";
		try {
			Class.forName(DRIVER_CLASS);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		// Buoc 2: Thong tin ket noi gom url, username, password
		String url = "jdbc:mysql://localhost:3306/mysql?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		String username = "root";
		String password = "";
				
		// Buoc 3: Thiet lap ket noi
		try {
			Connection con = DriverManager.getConnection(url, username, password);
			
			Statement statement = con.createStatement();
			
			String selectSQL = "select host, user from user";
			ResultSet result = statement.executeQuery(selectSQL);
					
			while (result.next()) {
				String host = result.getString(1);
				String user = result.getString("user");
				
				System.out.println(host + " --- " + user);
			}
			
			result.close();
			statement.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Connect fail.");
			e.printStackTrace();
		}
		

		System.out.println("Done.");
	}

}
