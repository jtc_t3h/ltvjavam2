package chapter5;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;

public class FrmBai3_ThemLienHe extends JFrame {

	private JPanel contentPane;
	private JTextField txtHoTen;
	private JTextField txtDTDD;
	private JTextField txtHinhAnh;
	private JLabel lblImage;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai3_ThemLienHe frame = new FrmBai3_ThemLienHe();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai3_ThemLienHe() {
		setTitle("Thêm mới liên hệ");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 522, 223);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Họ Tên");
		lblNewLabel.setBounds(10, 23, 80, 14);
		contentPane.add(lblNewLabel);
		
		txtHoTen = new JTextField();
		txtHoTen.setBounds(100, 20, 243, 20);
		contentPane.add(txtHoTen);
		txtHoTen.setColumns(10);
		
		JLabel lbltd = new JLabel("ĐTDĐ");
		lbltd.setBounds(10, 54, 80, 14);
		contentPane.add(lbltd);
		
		txtDTDD = new JTextField();
		txtDTDD.setColumns(10);
		txtDTDD.setBounds(100, 51, 243, 20);
		contentPane.add(txtDTDD);
		
		JLabel lblHnhnh = new JLabel("Hình ảnh");
		lblHnhnh.setBounds(10, 82, 80, 14);
		contentPane.add(lblHnhnh);
		
		txtHinhAnh = new JTextField();
		txtHinhAnh.setColumns(10);
		txtHinhAnh.setBounds(100, 79, 197, 20);
		contentPane.add(txtHinhAnh);
		
		JButton btnBrowser = new JButton("...");
		btnBrowser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Mở JFileChooser -> chọn hình
				JFileChooser fcs = new JFileChooser();
				
				File selected = null;
				if (fcs.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
					selected = fcs.getSelectedFile();
				}
				System.out.println(selected.getAbsolutePath());
				
				// Thiết lập đường dẫn hình vào txtHinhAnh và Hiển thị hình trong lblImage
				if (selected != null && (selected.getAbsolutePath().toLowerCase().endsWith(".png") || 
						selected.getAbsolutePath().toLowerCase().endsWith(".jpg") ||
						selected.getAbsolutePath().toLowerCase().endsWith(".gif"))) {
					txtHinhAnh.setText(selected.getAbsolutePath());
					
					lblImage.setIcon(new ImageIcon(selected.getAbsolutePath()));
				} else {
					JOptionPane.showMessageDialog(null, "Định dạng không hợp lệ.");
				}
			}
		});
		btnBrowser.setBounds(306, 78, 37, 23);
		contentPane.add(btnBrowser);
		
		JButton btnAdd = new JButton("Thêm mới");
		btnAdd.setBounds(100, 150, 105, 23);
		contentPane.add(btnAdd);
		
		lblImage = new JLabel("");
		lblImage.setBounds(353, 11, 142, 162);
		contentPane.add(lblImage);
	}
}
