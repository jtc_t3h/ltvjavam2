package chapter5;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class TryWithResouceDatabase {

	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/qllienhe";
		String usernameDB = "root";
		String passwordDB = "";
		
		String sql = "SELECT * FROM user";
		
		try (Connection con = DriverManager.getConnection(url, usernameDB, passwordDB);
				Statement statement = con.createStatement();
				ResultSet rs = statement.executeQuery(sql);
				) {
			while (rs.next()) {
				System.out.println(rs.getInt(1) + "\t\t" + rs.getString(2) + "\t\t" + rs.getString(3));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 

	}

}
