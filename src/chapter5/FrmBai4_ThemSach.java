package chapter5;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class FrmBai4_ThemSach extends JFrame {

	private JPanel contentPane;
	private JTextField txtName;
	private JTextField txtAuthor;
	private JTextField txtIsbn;
	private JTextField txtPrice;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrmBai4_ThemSach frame = new FrmBai4_ThemSach();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrmBai4_ThemSach() {
		setTitle("Thêm sách");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 508, 199);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Tên sách");
		lblNewLabel.setBounds(10, 11, 100, 14);
		contentPane.add(lblNewLabel);
		
		txtName = new JTextField();
		txtName.setBounds(120, 8, 370, 20);
		contentPane.add(txtName);
		txtName.setColumns(10);
		
		JLabel lblTcGi = new JLabel("Tác giả");
		lblTcGi.setBounds(10, 39, 100, 14);
		contentPane.add(lblTcGi);
		
		txtAuthor = new JTextField();
		txtAuthor.setColumns(10);
		txtAuthor.setBounds(120, 36, 370, 20);
		contentPane.add(txtAuthor);
		
		JLabel lblNxb = new JLabel("NXB");
		lblNxb.setBounds(10, 67, 100, 14);
		contentPane.add(lblNxb);
		
		txtIsbn = new JTextField();
		txtIsbn.setColumns(10);
		txtIsbn.setBounds(120, 64, 370, 20);
		contentPane.add(txtIsbn);
		
		JLabel lblGiBa = new JLabel("Giá bìa");
		lblGiBa.setBounds(10, 95, 100, 14);
		contentPane.add(lblGiBa);
		
		txtPrice = new JTextField();
		txtPrice.setColumns(10);
		txtPrice.setBounds(120, 92, 370, 20);
		contentPane.add(txtPrice);
		
		JButton btnNewButton = new JButton("Thêm sách mới");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				///
				String name = txtName.getText().trim();
				String author = txtAuthor.getText().trim();
				String isbn = txtIsbn.getText().trim();
				double price = Double.parseDouble(txtPrice.getText().trim());
				
				Bai456_Sach sach = new Bai456_Sach(name, author, isbn, price);
				
				///
				Bai456_DatabaseUtil db = new Bai456_DatabaseUtil();
				try {
					db.add(sach);
					JOptionPane.showMessageDialog(rootPane,"Da Them Sach Moi!");
				} catch (SQLException e) {
					JOptionPane.showMessageDialog(rootPane, e.getMessage());
				}
			}
		});
		btnNewButton.setBounds(10, 136, 114, 23);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Tiếp tục");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txtName.setText("");
				txtAuthor.setText("");
				txtIsbn.setText("");
				txtPrice.setText("");
			}
		});
		btnNewButton_1.setBounds(153, 136, 91, 23);
		contentPane.add(btnNewButton_1);
	}
}
