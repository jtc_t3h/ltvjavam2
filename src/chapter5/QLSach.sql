CREATE DATABASE QLSach CHARACTER SET utf8 COLLATE utf8_general_ci;

--use QLSach;

CREATE TABLE sach (
  id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(200),
  author VARCHAR(200),
  isbn VARCHAR(200),
  price DOUBLE,
  primary key (id)
);

