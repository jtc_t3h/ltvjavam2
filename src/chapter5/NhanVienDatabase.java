package chapter5;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class NhanVienDatabase {

	public static void main(String[] args) {
		NhanVienDatabase db = new NhanVienDatabase();
		
		NhanVien nv = new NhanVien();
		nv.setId(1);
		nv.setHoTen("Pham Duc De");
		db.update(nv);
		
		List<NhanVien> list = db.list();
		System.out.println(list.size());
		for (NhanVien nv1: list) {
			System.out.println(nv1.getHoTen());
		}
	}

	/**
	 * Lay danh sach nhan vien hien dang co trong database
	 */
	public List<NhanVien> list() {

		// B1: Kiem tra da import thu vien hay chua? -> ung dung Web (bat buoc phai co)
		final String JDBC_DRIVER_CLASS = "com.mysql.cj.jdbc.Driver"; // khac nhau giua cac DBMS
		try {
			Class.forName(JDBC_DRIVER_CLASS);
			System.out.println("Imported MySQL Connector J");
		} catch (ClassNotFoundException e) {
			System.out.println("UnImport MySQL Connector J");
		}

		// B2: chuoi ket noi -> phu thuoc vao DBMS
		String JDBC_URL = "jdbc:mysql://localhost:3306/phan_cong_nhan_vien_1_1_nam";

		// B3: Tao ket noi
		String JDBC_USERNAME = "root";
		String JDBC_PASSWORD = "";
		Connection con = null;
		
		List<NhanVien> list = new ArrayList<NhanVien>();
		try {
			con = DriverManager.getConnection(JDBC_URL, JDBC_USERNAME, JDBC_PASSWORD);
			System.out.println("connect successful.");

			// B4. Tao doi tuong Statement -> quan ly thuc thi truy van -> executeQuery |
			// executeUpdate
			Statement st = con.createStatement();

			// B5. Thuc thi truy van
			String sql = "SELECT ID, Ho_ten, Gioi_tinh, Ngay_sinh FROM nhan_vien";
			ResultSet rs = st.executeQuery(sql);

			// B6: Xu ly ket qua truy van
			while (rs.next()) {
				NhanVien nv = new NhanVien();

				nv.setId(rs.getInt(1));
				nv.setHoTen(rs.getString("Ho_ten"));
				nv.setGioiTinh(rs.getString(3));
				nv.setNgaySinh(rs.getDate("Ngay_sinh"));

				list.add(nv);
			}

			// B7: Dong ket noi
			rs.close();
			st.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Connect fail.");
		}
		return list;
	}

	/**
	 * Neu return > 0 -> update thanh cong, nguoc lai la loi
	 * @param nv
	 * @return
	 */
	public int update(NhanVien nv) {
		String JDBC_URL = "jdbc:mysql://localhost:3306/phan_cong_nhan_vien_1_1_nam";
		String JDBC_USERNAME = "root";
		String JDBC_PASSWORD = "";
		
		int rt = 0;
		Connection con = null;
		try {
			con = DriverManager.getConnection(JDBC_URL, JDBC_USERNAME, JDBC_PASSWORD);
			Statement st = con.createStatement();
			String sql = "update nhan_vien set ho_ten = '" + nv.getHoTen() + "' where id = " + nv.getId();
			rt = st.executeUpdate(sql);
			
			st.close();
			con.close();
		} catch (SQLException e) {
			System.out.println("Connect fail.");
		}
		return rt;
	}
	
	public int update(List<NhanVien> list) throws SQLException {
		String JDBC_URL = "jdbc:mysql://localhost:3306/phan_cong_nhan_vien_1_1_nam";
		String JDBC_USERNAME = "root";
		String JDBC_PASSWORD = "";
		
		int rt = 0;
		Connection con = null;
		try {
			con = DriverManager.getConnection(JDBC_URL, JDBC_USERNAME, JDBC_PASSWORD);
			PreparedStatement st = con.prepareStatement("update nhan_vien set ho_ten = ? where id = ?");
			
			con.setAutoCommit(false);
			for (NhanVien nv: list) {
				st.setInt(1, nv.getId());
				st.setString(2, nv.getHoTen());
				rt += st.executeUpdate();
			}
			con.commit();
			st.close();
			con.close();
			
		} catch (SQLException e) {
			con.rollback();
			System.out.println("Connect fail.");
		}
		return rt;
	}
	
}
