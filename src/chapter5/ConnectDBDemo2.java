package chapter5;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectDBDemo2 {

	public static void main(String[] args) throws SQLException {

		// Buoc 1: nap trinh dieu kien -> import thu vien
		// Cach kiem tra thu vien da import hay chua
		final String DRIVER_CLASS = "com.mysql.cj.jdbc.Driver";
		try {
			Class.forName(DRIVER_CLASS);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		// Buoc 2: Thong tin ket noi gom url, username, password
		String url = "jdbc:mysql://localhost:3306/phan_cong_nhan_vien_1_1_nam?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		String username = "root";
		String password = "";
				
		// Buoc 3: Thiet lap ket noi
		Connection con = null;
		Statement statement = null;
		ResultSet result = null;
		try {
			con = DriverManager.getConnection(url, username, password);
			
			statement = con.createStatement();
			
			String selectSQL = "select Ho_ten, Ngay_sinh, Dia_chi from nhan_vien";
			result = statement.executeQuery(selectSQL);
					
			while (result.next()) {
				String ho_ten = result.getString(1);
				String ngay_sinh = result.getString("Ngay_sinh");
				String dia_chi = result.getString("Dia_chi");
				
				System.out.println(ho_ten + " --- " + ngay_sinh + " --- " + dia_chi);
			}
		} catch (SQLException e) {
			System.out.println("Connect fail.");
			e.printStackTrace();
		} finally {
			if (result != null) result.close();
			if (statement != null) statement.close();
			if (con != null) con.close();
		}
		

		System.out.println("Done.");
	}

}
