package chapter5;

public class Bai3_LienHe {

	private String hoten;
	private String dtdd;
	private String hinhanh;

	public Bai3_LienHe(String hoten, String dtdd, String hinhanh) {
		this.hoten = hoten;
		this.dtdd = dtdd;
		this.hinhanh = hinhanh;
	}

	public Bai3_LienHe() {
	}

	public String getHoten() {
		return hoten;
	}

	public void setHoten(String hoten) {
		this.hoten = hoten;
	}

	public String getDtdd() {
		return dtdd;
	}

	public void setDtdd(String dtdd) {
		this.dtdd = dtdd;
	}

	public String getHinhanh() {
		return hinhanh;
	}

	public void setHinhanh(String hinhanh) {
		this.hinhanh = hinhanh;
	}

}
