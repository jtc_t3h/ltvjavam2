package chapter7;

public class ConcreteObserverA extends Observer {

	
	public ConcreteObserverA(Subject subject) {
		subject.registerObserver(this);
	}
	
	@Override
	public void update() {
		System.out.println("ConcreteObserverA: update method.");
	}

}
