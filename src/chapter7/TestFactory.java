package chapter7;

public class TestFactory {

	public static void main(String[] args) {

		Creator creator = new ConcreteCreatorA();
		creator.factoryMethod();
	}

}
