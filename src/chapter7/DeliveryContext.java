package chapter7;

public class DeliveryContext {

	private DeliveryStrategy deliveryStrategy;
	
	public DeliveryContext(DeliveryStrategy deliveryStrategy) {
		this.deliveryStrategy = deliveryStrategy;
	}
	
	public void executeDelivery() {
		deliveryStrategy.delivery();
	}
}
