package chapter7;

public class ConcreteCreatorB implements Creator {

	@Override
	public void factoryMethod() {
		ConcreteProductB product = new ConcreteProductB();
		product.create();
	}

}
