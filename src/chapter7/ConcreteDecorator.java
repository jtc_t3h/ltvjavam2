package chapter7;

public class ConcreteDecorator extends Decorator {
	
	public ConcreteDecorator(Component component) {
		this.component = component; 
	}

	@Override
	public void operation() {
		component.operation();
	}

	public void addBehavior() {
		System.out.println("ConcreteDecorator: addBehavior method.");
	}
}
