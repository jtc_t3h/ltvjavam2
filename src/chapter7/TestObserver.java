package chapter7;

public class TestObserver {

	public static void main(String[] args) {
		Subject subject = new Subject();
		
		Observer observerA = new ConcreteObserverA(subject);
		Observer observerB = new ConcreteObserverB(subject);
				
//		subject.registerObserver(observerA);
//		subject.registerObserver(observerB);
		subject.setState(10);
		
	}

}
