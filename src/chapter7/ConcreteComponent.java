package chapter7;

public class ConcreteComponent implements Component {

	@Override
	public void operation() {
		System.out.println("ConcreteComponent: operation method.");
	}
	

}
