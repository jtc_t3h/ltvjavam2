package chapter7;

public class QuickDeliveryStrategy implements DeliveryStrategy {

	@Override
	public void delivery() {
		System.out.println("QuickDeliveryStrategy: delivery method.");
	}

}
