package chapter7;

public class ConcreteObserverB extends Observer{

	public ConcreteObserverB(Subject subject) {
		subject.registerObserver(this);
	}
	
	@Override
	public void update() {
		System.out.println("ConcreteObserverB: update method.");
	}
}
