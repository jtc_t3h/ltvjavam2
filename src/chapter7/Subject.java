package chapter7;

import java.util.ArrayList;
import java.util.List;

public class Subject {

	private int state;
	private List<Observer> observerCollection;
	
	public Subject() {
		this.observerCollection = new ArrayList<Observer>();
	}
	
	public void registerObserver(Observer observer) {
		observerCollection.add(observer);
	}
	
	public void unregisterObserver(Observer observer) {
		observerCollection.remove(observer);
	}
	
	public void notityObserver() {
		for (Observer observer: observerCollection) {
			observer.update();
		}
	}

	public void setState(int state) {
		this.state = state;
		notityObserver();
	}
	
	
}
