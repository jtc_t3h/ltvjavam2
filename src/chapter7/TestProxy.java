package chapter7;

public class TestProxy {

	public static void main(String[] args) {
		
		Service service = new RealService();
//		service.defaultMethod();
		
		Service service2 =  new Proxy((RealService) service);
		service2.defaultMethod();
	}

}
