package chapter7;

public class TestBuilder {

	public static void main(String[] args) {
		Car car = new Car();
		car.buildColor("red").buildWheels(4);
	}

}
