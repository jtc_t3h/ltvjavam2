package chapter7;

public class Car {

	private String color;
	private int wheels;
	private String mirrors;
	
	public Car buildColor(String color) {
		this.color = color;
		return this;
	}
	
	public Car buildWheels(int wheels) {
		this.wheels = wheels;
		return this;
	}
}
