package chapter7;

public class ConcreteCreatorA implements Creator {

	@Override
	public void factoryMethod() {
		ConcreteProductA product = new ConcreteProductA();
		product.create();
	}

}
