package chapter7;

public class TestStrategy {

	public static void main(String[] args) {

		// Doi tuong strategy thay doi theo nguoi dung
		DeliveryStrategy strategy = new StandardDeliveryStrategy();
		
		
		DeliveryContext context = new DeliveryContext(strategy);
		context.executeDelivery();
	}

}
