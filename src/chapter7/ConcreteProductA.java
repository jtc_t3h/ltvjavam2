package chapter7;

public class ConcreteProductA implements Product {

	@Override
	public void create() {
		System.out.println("ConcreteProductA: create method.");
	}

}
