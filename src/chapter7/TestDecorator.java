package chapter7;

public class TestDecorator {

	public static void main(String[] args) {
		Component component = new ConcreteComponent();
		Decorator decorator = new ConcreteDecorator(component);
		
		decorator.operation();
		decorator.addBehavior();
	}

}
