package chapter7;

public class Singleton {

	private Singleton() {
	}
	
	private static Singleton instance;
	
	public static Singleton getInstance() {
		if (instance == null) {
			instance = new Singleton();
		}
		return instance;
	}
	
	// Thuoc tinh khac
	private int state = 0;

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}
	
}
