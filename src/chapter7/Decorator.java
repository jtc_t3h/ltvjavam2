package chapter7;

public abstract class Decorator implements Component{

	protected Component component;
	
	public abstract void addBehavior();
}
