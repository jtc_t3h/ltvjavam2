package chapter7;

public class StandardDeliveryStrategy implements DeliveryStrategy{

	@Override
	public void delivery() {
		System.out.println("StandardDeliveryStrategy: delivery method.");
	}

}
