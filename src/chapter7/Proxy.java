package chapter7;

public class Proxy implements Service {

	private RealService service;

	public Proxy(RealService service) {
		super();
		this.service = service;
	}



	@Override
	public void defaultMethod() {
		// Thuc hien kiem tra ...
		service.defaultMethod();
	}
	
	
}
