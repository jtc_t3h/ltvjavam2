package chapter7;

public class TestSingleton {

	public static void main(String[] args) {
		Singleton singleton1 = Singleton.getInstance();
		Singleton singleton2 = Singleton.getInstance();
		
		singleton1.setState(10);
		System.out.println(singleton2.getState()); // 10
	}

}
